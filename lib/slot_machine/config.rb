# Config where is store all informations
module SlotMachine::Config
  def self.read_slots(custom_path = nil)
    globs = (custom_path || path)
    globs = [globs] unless globs.is_a?(Array)

    paths_array = globs.map do |g|
      Dir[g]
    end.flatten

    slots_array = paths_array.map do |p|
      basename = File.basename(p, File.extname(p))
      basename = basename.split('.').first
      basename.slice!(0)
      method_name = basename.parameterize.underscore.to_sym
      partial_path = File.dirname(p).split('/views/').last
      partial = File.join(partial_path, basename)
      [method_name, partial]
    end

    @slots = Hash[slots_array]
  end

  def self.method_names
    @slots.keys
  end

  def self.partial(method_name)
    @slots[method_name]
  end

  def self.path
    @path || Rails.root.join('app/views/slots/_*')
  end

  def self.helper_modules
    @helper_modules ||= {}
  end

  def self.helper_modules=(val)
    @helper_modules = val
  end
end

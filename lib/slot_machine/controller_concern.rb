# Helper to be included in application_controller
module SlotMachine::ControllerConcern
  # In the the view:
  # `= foo label: 'toto' do 'something awesome' end`
  #
  # In the partial foo:
  # `slots[:label] == label == 'toto'`
  # `slots[:main] == main == 'something awesome'`
  #
  # so you should use `slots[:something]` when it could be unassigned
  extend ActiveSupport::Concern

  # A builder class base for the builder of the different partials
  class Builder
    attr_internal :slots

    def initialize(template)
      @template = template
    end

    # attribute reader
    def slots
      @slots ||= Hash.new { |h, k| h[k] = ActiveSupport::SafeBuffer.new('') }
    end

    def slot?(name)
      !slots[name].blank?
    end

    # append the named slot
    def append_slot(name = :main, string = nil, &block)
      slots[name].concat(@template.get_buffer(string, &block))
      nil # why the fuck do I need this. It shouldn't put anything in the buffer but it does.
    end
    alias_method :slot, :append_slot
  end

  class_methods do
    # create the module to be called as a helper
    # Typically :
    # ```
    #   include SlotMachine::ControllerConcern
    #   helper slot_machine_create_module(my_path)
    # ````
    def slot_machine_helper(path: nil, name: :main)
      return SlotMachine::Config.helper_modules[name] unless SlotMachine::Config.helper_modules[name].nil?

      # Read slot on the custom path
      slots = SlotMachine::Config.read_slots(path)
      method_names = slots.keys

      # Instantiate the new module
      SlotMachine::Config.helper_modules[name] = Module.new

      # Create a child builder class with all the methods
      builder_class = Class.new(Builder)

      builder_class.class_eval do
        method_names.each do |method_name|
          define_method(method_name) do |args = { slot: :main }, &block|
            slot_name = args.delete(:slot)
            rendered_partial = @template.send(method_name, args, &block)
            append_slot(slot_name, rendered_partial)
          end
        end
      end

      # Add 1 method per partial in the module as well
      method_names.each do |method_name|
        SlotMachine::Config.helper_modules[name].module_eval do
          # this one should be in the builder but it doesnt have the builder method
          def get_buffer(string = nil, &block)
            return string unless block

            capture { yield }
          end
          define_method(method_name) do |locals = {}, &block|
            # that is some weird shit but il allow to access all the local_assigns
            # within the slots and still access the directly
            locals[:slots] = locals

            # Here we create the builder
            # we execute the block while passing the builder
            # all the slotted partial will be in the builder
            # and activeview buffer is captured and slotted
            # in the main slot
            if block
              builder = builder_class.new(self)
              captured = capture { block.call(builder) }
              builder.append_slot(:main, captured)
              locals[:slots] = locals[:slots].merge(builder.slots)
              locals[:m] = builder
            end
            render slots[method_name], locals
          end
        end
      end

      SlotMachine::Config.helper_modules[name]
    end
  end
end

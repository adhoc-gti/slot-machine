$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'slot_machine/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'slotmachine'
  s.version     = SlotMachine::VERSION
  s.authors     = ['Cedric Feyaerts']
  s.email       = ['c.feyaerts@adhoc-gti.com']
  s.homepage    = 'https://gitlab.com/cedricfeyaerts/slot-machine'
  s.summary     = 'A Simple gem to write readable views'
  s.description = 'Hides the ugly css and html from your views'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'rails', '> 4.0.0'
end
